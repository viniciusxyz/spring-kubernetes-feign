package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.proxy.ProxyGenerico;

@RestController
public class RestTest {
	
	@Autowired
	private DiscoveryClient discoveryClient;
	
	@Autowired
	private ProxyGenerico proxyGenerico;

	@GetMapping("/services")
	public ResponseEntity<?> services()  {
		return new ResponseEntity<Object>(discoveryClient.getServices(), HttpStatus.OK);
	}
	
	@GetMapping("/order")
	public ResponseEntity<?> order()  {
		
		return new ResponseEntity<Object>(discoveryClient.getOrder(), HttpStatus.OK);
	}
	
	@GetMapping("/gitlab")
	public ResponseEntity<?> gitlab()  {
		
		return new ResponseEntity<Object>(proxyGenerico.getHttpResponse(), HttpStatus.OK);
	}
	
}
