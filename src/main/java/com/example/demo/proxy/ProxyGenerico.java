package com.example.demo.proxy;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("gitlab")
@RibbonClient(name="gitlab")
public interface ProxyGenerico {
	
	@RequestMapping(method = RequestMethod.GET)
    String getHttpResponse();

}
